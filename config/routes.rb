Rails.application.routes.draw do
  resources :featureimages
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  resources :homes
  root "homes#index"

  resources :categoryblogs do
    resources :blogs
  end

  resources :blogs
  resources :portfolios
  resources :categoryportfolios do
    resources :portfolios
  end


end
