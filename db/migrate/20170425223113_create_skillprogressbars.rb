class CreateSkillprogressbars < ActiveRecord::Migration[5.0]
  def change
    create_table :skillprogressbars do |t|
      t.string :skillname
      t.integer :progressbar
      t.string :level

      t.timestamps
    end
  end
end
