class AddAttachmentImageToFeatureimages < ActiveRecord::Migration
  def self.up
    change_table :featureimages do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :featureimages, :image
  end
end
