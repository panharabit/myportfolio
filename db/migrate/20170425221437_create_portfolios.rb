class CreatePortfolios < ActiveRecord::Migration[5.0]
  def change
    create_table :portfolios do |t|
      t.string :title
      t.date :release
      t.string :client
      t.string :livedemo
      t.date :post_date
      t.string :skill
      t.text :description
      t.string :image
      t.references :categoryportfolio, foreign_key: true

      t.timestamps
    end
  end
end
