class CreateAbouts < ActiveRecord::Migration[5.0]
  def change
    create_table :abouts do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.date :dateofbirth
      t.string :address
      t.string :nationality
      t.text :description
      t.string :image

      t.timestamps
    end
  end
end
