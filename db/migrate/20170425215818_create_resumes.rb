class CreateResumes < ActiveRecord::Migration[5.0]
  def change
    create_table :resumes do |t|
      t.string :title
      t.string :place
      t.string :dateofjob
      t.text :description
      t.string :image

      t.timestamps
    end
  end
end
