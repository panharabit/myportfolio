class CreateCategoryportfolios < ActiveRecord::Migration[5.0]
  def change
    create_table :categoryportfolios do |t|
      t.string :categoryname

      t.timestamps
    end
  end
end
