class AddOtherFieldToCategoryportfolio < ActiveRecord::Migration[5.0]
  def change
    add_column :categoryportfolios, :slug, :string
    add_index :categoryportfolios, :slug, unique: true
  end
end
