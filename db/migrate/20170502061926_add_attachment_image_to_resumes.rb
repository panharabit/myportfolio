class AddAttachmentImageToResumes < ActiveRecord::Migration
  def self.up
    change_table :resumes do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :resumes, :image
  end
end
