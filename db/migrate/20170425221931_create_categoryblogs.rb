class CreateCategoryblogs < ActiveRecord::Migration[5.0]
  def change
    create_table :categoryblogs do |t|
      t.string :categoryname

      t.timestamps
    end
  end
end
