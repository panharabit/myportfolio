class AddOtherFieldToCategoryblog < ActiveRecord::Migration[5.0]
  def change
    add_column :categoryblogs, :slug, :string
    add_index :categoryblogs, :slug, unique: true
  end
end
