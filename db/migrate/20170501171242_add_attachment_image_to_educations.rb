class AddAttachmentImageToEducations < ActiveRecord::Migration
  def self.up
    change_table :educations do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :educations, :image
  end
end
