class CreateTestimonials < ActiveRecord::Migration[5.0]
  def change
    create_table :testimonials do |t|
      t.string :name
      t.string :image
      t.text :description
      t.string :postion

      t.timestamps
    end
  end
end
