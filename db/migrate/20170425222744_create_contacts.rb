class CreateContacts < ActiveRecord::Migration[5.0]
  def change
    create_table :contacts do |t|
      t.string :saysomething
      t.string :location
      t.string :website
      t.string :phone
      t.string :email
      t.text :description

      t.timestamps
    end
  end
end
