class CreateSearchengines < ActiveRecord::Migration[5.0]
  def change
    create_table :searchengines do |t|
      t.string :title
      t.text :description
      t.text :keyword

      t.timestamps
    end
  end
end
