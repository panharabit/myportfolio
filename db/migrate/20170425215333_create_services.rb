class CreateServices < ActiveRecord::Migration[5.0]
  def change
    create_table :services do |t|
      t.string :servicename
      t.text :description
      t.string :image

      t.timestamps
    end
  end
end
