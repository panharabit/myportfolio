class CreateEducations < ActiveRecord::Migration[5.0]
  def change
    create_table :educations do |t|
      t.string :university
      t.string :title
      t.date :startdate
      t.date :finisheddate
      t.text :description
      t.string :image

      t.timestamps
    end
  end
end
