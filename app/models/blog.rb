class Blog < ApplicationRecord
  before_save :update_slug

  def update_slug
    self.slug = title.parameterize
  end

  def to_param
    slug
  end

  has_attached_file :image, styles: {
medium: "300x300>",
thumb: "100x100>"
}
validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
belongs_to :categoryblog
has_many :featureimages

    # def to_param
    #  "#{permalink}"
    # end
    #
    # def permalink
    #  permalink = self.title.parameterize
    # end
    # extend FriendlyId
    # friendly_id :title, use: :slugged
end
