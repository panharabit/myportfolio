class Categoryblog < ApplicationRecord
  has_many :blogs

  extend FriendlyId
  friendly_id :categoryname, use: :slugged

  def to_param
    "#{permalink}"
  end

  def permalink
    permalink = self.categoryname.parameterize
  end
end
