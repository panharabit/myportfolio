class Categoryportfolio < ApplicationRecord
  has_many :portfolios

  extend FriendlyId
  friendly_id :categoryname, use: :slugged

  def to_param
    "#{permalink}"
  end

  def permalink
    permalink = self.categoryname.parameterize
  end
end
