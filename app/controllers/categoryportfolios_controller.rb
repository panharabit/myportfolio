class CategoryportfoliosController < ApplicationController

  def show
    @contacts = Contact.all.limit(1)
    @portfolios = Portfolio.all
    # @portfolio = Portfolio.find(params[:id])
    @categoryportfolio = Categoryportfolio.friendly.find(params[:id])
    @portfolios = @categoryportfolio.portfolios
    @searchengines = Searchengine.all
    # @searchengine = Searchengine.find(params[:id])
  end
end
