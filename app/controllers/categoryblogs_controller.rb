class CategoryblogsController < ApplicationController
  def show
    @contacts = Contact.all.limit(1)
    @categoryblogs = Categoryblog.all
    # @blog = Blog.friendly.find(params[:id])
    @categoryblog = Categoryblog.friendly.find(params[:id])
    @blogs = @categoryblog.blogs

    @searchengines = Searchengine.all
  end
end
