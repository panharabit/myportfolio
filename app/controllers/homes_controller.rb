class HomesController < ApplicationController
  def index
    @about = About.all.limit(1)
    @services = Service.all.limit(4)
    @resumes = Resume.all.limit(4)
    @educations = Education.all.limit(4)
    @skills = Skill.all.limit(1)
    @skillprogressbars = Skillprogressbar.limit(4)
    @sectionlanguage = Additionalsection.where({title: ["language skills"]})
    @sectionknowlegde = Additionalsection.where({title: ["knowlegde"]})
    @skilllanguages = Skillprogressbar.where({id: [5,6,7,8]})
    @categoryportfolio = Categoryportfolio.all
    @portfolios = Portfolio.all
    @blogs = Blog.all.limit(4)
    @contacts = Contact.all.limit(1)
    @searchengines = Searchengine.all
  end
end
