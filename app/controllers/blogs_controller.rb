class BlogsController < ApplicationController

  def index
    @blogs = Blog.all
    @contacts = Contact.all
    @searchengines = Searchengine.all.where(title: "Blog | panhara.work").limit(1)
  end

  def show
    # @categoryblog = Categoryblog.find(params[:id])
    # @blogs = @categoryblog.blogs
    # @categoryblog = Categoryblog.find(params[:id])
    @blog = Blog.find_by_slug(params[:id])

    @blogs = Blog.all
    @categoryblogs = Categoryblog.all
    @contacts = Contact.all
    @featureimage = Blog.find_by_slug(params[:id])
    @bl = @featureimage.featureimages
  end

end
