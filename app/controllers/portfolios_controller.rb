class PortfoliosController < ApplicationController
  def index
    @contacts = Contact.all.limit(1)
    @portfolios = Portfolio.all
    @searchengines = Searchengine.all.where(title: "Portfolio | panhara.work")

  end


  def show

    @contacts = Contact.all.limit(1)
    @portfolio = Portfolio.find_by_slug(params[:id])
    @searchengines = Searchengine.all
  end
end
