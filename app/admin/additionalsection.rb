ActiveAdmin.register Additionalsection do
  permit_params :title,:description

  show do |t|
    attributes_table do
      row :title
      row :description do |d|
        d.description
      end
    end
  end




    form :html=> {:enctype=>"multipart/form-data"} do |f|
    f.inputs do
      f.input :title
      f.input :description, :as => :ckeditor
      actions
    end

  end

  index do
    selectable_column
    column link_to "ID" do |section|
      link_to section.id,admin_additionalsection_path(section)
    end
    column :title
    column :description, :sortable => :description do |description|
      div do
        truncate(strip_tags(description.description))
      end
    end
    column :created_at
    column :updated_at
    actions
  end

end
