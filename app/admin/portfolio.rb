ActiveAdmin.register Portfolio do
menu parent: "Manage Content"
permit_params :title,:release,:client,:livedemo,:post_date,:skill,:description,:image,:author,:categoryportfolio_id

  controller do
    def find_resource
    scoped_collection.find_by_slug(params[:id])
    end
  end

  show do |t|
    attributes_table do
      row :title
      row :post_date
      row :description do |d|
        d.description
      end
      row :client
      row :livedemo
      row :post_date
      row :skill
      row :author
      row :image do
        portfolio.image? ? image_tag(portfolio.image.url, height: '100') : content_tag(:span, "No photo yet")
      end
    end
  end


  form :html=> {:enctype=>"multipart/form-data"} do |f|
  f.inputs do
    f.collection_select :categoryportfolio_id,Categoryportfolio.all,:id,:categoryname
    f.input :title
    f.input :author
    f.input :post_date, as: :datepicker
    f.input :client
    f.input :livedemo
    f.input :skill
    f.input :description, :as => :ckeditor

    f.file_field :image, hint: f.portfolio.image? ? image_tag(portfolio.image.url, height:'100') : content_tag(:span,"Upload JPG/PNG/GIF image")
    actions
  end

end

index do
  selectable_column
  column link_to "ID" do |portfolio|
    link_to portfolio.id,admin_portfolio_path(portfolio)
  end
  column :title
  column :author
  column :description, :sortable => :description do |description|
    div do
      truncate(strip_tags(description.description))
    end
  end
  column :post_date
  column :client
  column :skill
  column :livedemo
  column link_to "Image File" do |portfolio|
    # image_tag(post.image_file_name)
    image_tag(portfolio.image.url, height:'100')
  end
  column :created_at
  column :updated_at
  actions
end
end
