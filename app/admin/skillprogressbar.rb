ActiveAdmin.register Skillprogressbar do
menu parent: "Manage Content"
permit_params :skillname,:progressbar,:level

show do |t|
  attributes_table do
    row :skillname
    row :progressbar
    row :level

  end
end




  form :html=> {:enctype=>"multipart/form-data"} do |f|
  f.inputs do
    f.input :skillname
    f.input :progressbar
    f.input :level
    actions
  end

end

index do
  selectable_column
  column link_to "ID" do |skillpro|
    link_to skillpro.id,admin_skillprogressbar_path(skillpro)
  end
  column :skillname
  column :progressbar
  column :level
  column :created_at
  column :updated_at
  actions
end
end
