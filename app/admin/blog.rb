ActiveAdmin.register Blog do
menu parent: "Manage Content"
permit_params :title,:description,:image,:post_date,:author,:categoryblog_id

  controller do
    def find_resource
      scoped_collection.find_by_slug(params[:id])
    end
  end

  show do |t|
    attributes_table do
      row :title
      row :post_date
      row :author
      row :description do |d|
        raw d.description
      end
      row :image do
        blog.image? ? image_tag(blog.image.url, height: '100') : content_tag(:span, "No photo yet")
      end
    end
  end


  form :html=> {:enctype=>"multipart/form-data"} do |f|
  f.inputs do
    f.collection_select :categoryblog_id,Categoryblog.all,:id,:categoryname
    f.input :title
    f.input :post_date, as: :datepicker
    f.input :author
    f.input :description, :as => :ckeditor
    f.file_field :image, hint: f.blog.image? ? image_tag(blog.image.url, height:'100') : content_tag(:span,"Upload JPG/PNG/GIF image")
    actions
  end

end

index do
  selectable_column
  actions
  column link_to "ID" do |blog|
    link_to blog.id,admin_blog_path(blog)
  end
  column :title
  column link_to "Description" do |blog|
  raw blog.description.truncate(200)
  end
  column :post_date
  column link_to "Image File" do |blog|
    # image_tag(post.image_file_name)
    image_tag(blog.image.url, height:'100')
  end
  column :created_at
  column :updated_at
end
end
