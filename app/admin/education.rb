ActiveAdmin.register Education do
menu parent: "Manage Content"


permit_params :university,:title,:startdate,:finisheddate,:description,:image

show do |t|
  attributes_table do
    row :university
    row :title
    row :startdate
    row :finisheddate
    row :description do |d|
      d.description
    end
    row :image do
      education.image? ? image_tag(education.image.url, height: '100') : content_tag(:span, "No photo yet")
    end
  end
end




  form :html=> {:enctype=>"multipart/form-data"} do |f|
  f.inputs do
    f.input :university
    f.input :title
    f.input :startdate, as: :datepicker
    f.input :finisheddate, as: :datepicker
    f.input :description, :as => :ckeditor
    f.file_field :image, hint: f.education.image? ? image_tag(education.image.url, height:'100') : content_tag(:span,"Upload JPG/PNG/GIF image")
    actions
  end

end

index do
  selectable_column
  column link_to "ID" do |edu|
    link_to edu.id,admin_education_path(edu)
  end
  column :university
  column :title
  column :startdate
  column :finisheddate
  column :description, :sortable => :description do |description|
    div do
      truncate(strip_tags(description.description))
    end
  end
  column link_to "Image File" do |edu|
    # image_tag(post.image_file_name)
    image_tag(edu.image.url, height:'100')
  end
  column :created_at
  column :updated_at
  actions
end
end
