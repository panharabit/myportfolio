ActiveAdmin.register Searchengine do
  permit_params :title,:description,:keyword

  show do |t|
    attributes_table do
      row :title
      row :keyword do |k|
        k.keyword
      end
      row :description do |d|
        strip_tags(d.description)
      end
    end
  end




    form :html=> {:enctype=>"multipart/form-data"} do |f|
    f.inputs do
      f.input :title
      f.input :keyword, :as => :ckeditor
      f.input :description, :as => :ckeditor
      actions
    end

  end

  index do
    selectable_column
    column link_to "ID" do |search|
      link_to search.id,admin_searchengine_path(search)
    end
    column :title
    column :keyword, :sortable => :description do |keyword|
      div do
        truncate(strip_tags(keyword.keyword))
      end
    end
    column :description, :sortable => :description do |description|
      div do
        truncate(strip_tags(description.description))
      end
    end
    column :created_at
    column :updated_at
    actions
  end

end
