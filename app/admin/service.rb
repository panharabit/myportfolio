ActiveAdmin.register Service do
menu parent: "Manage Content"
permit_params :servicename,:description

show do |t|
  attributes_table do
    row :servicename
    row :description do |d|
      d.description
    end
  end
end




  form :html=> {:enctype=>"multipart/form-data"} do |f|
  f.inputs do
    f.input :servicename
    f.input :description, :as => :ckeditor
    actions
  end

end

index do
  selectable_column
  column link_to "ID" do |service|
    link_to service.id,admin_service_path(service)
  end
  column :servicename
  column :description, :sortable => :description do |description|
    div do
      truncate(strip_tags(description.description))
    end
  end
  column :created_at
  column :updated_at
  actions
end
end
