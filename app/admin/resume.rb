ActiveAdmin.register Resume do
menu parent: "Manage Content"
permit_params :place,:dateofjob,:title,:description,:image

show do |t|
  attributes_table do

    row :place
    row :dateofjob
    row :title
    row :description do |d|
      d.description
    end
    row :image do
      resume.image? ? image_tag(resume.image.url, height: '100') : content_tag(:span, "No photo yet")
    end
  end
end




  form :html=> {:enctype=>"multipart/form-data"} do |f|
  f.inputs do

    f.input :place
    f.input :dateofjob, as: :datepicker
    f.input :title
    f.input :description, :as => :ckeditor
    f.file_field :image, hint: f.resume.image? ? image_tag(resume.image.url, height:'100') : content_tag(:span,"Upload JPG/PNG/GIF image")
    actions
  end

end

index do
  selectable_column
  column link_to "ID" do |resume|
    link_to resume.id,admin_resume_path(resume)
  end

  column :place
  column :dateofjob
  column :title
  column :description, :sortable => :description do |description|
    div do
      truncate(strip_tags(description.description))
    end
  end
  column link_to "Image File" do |resume|
    # image_tag(post.image_file_name)
    image_tag(resume.image.url, height:'100')
  end
  column :created_at
  column :updated_at
  actions
end
end
