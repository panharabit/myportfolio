ActiveAdmin.register About do
menu parent: "Manage Content"
  permit_params :name,:email,:phone,:dateofbirth,:address,:nationality,:description,:image
    show do |t|
      attributes_table do
        row :name
        row :email
        row :phone
        row :dateofbirth
        row :address
        row :nationality
        row :description do |d|
          raw d.description
        end
        row :image do
          about.image? ? image_tag(about.image.url, height: '100') : content_tag(:span, "No photo yet")
        end
      end
    end

    filter :title

    form :html=> {:enctype=>"multipart/form-data"} do |f|
      f.inputs do
        f.input :name
        f.input :email
        f.input :phone
        f.input :dateofbirth , as: :datepicker
        f.input :address
        f.input :nationality
        f.input :description , :as => :ckeditor
        f.file_field :image, hint: f.about.image? ? image_tag(about.image.url, height:'100') : content_tag(:span,"Upload JPG/PNG/GIF image")
        actions
      end

    end

    index do
      selectable_column
      column :name
      column :email
      column :phone
      column :dateofbirth
      column :address
      column :nationality
      column :description, :sortable => :description do |description|
        div do
          truncate(strip_tags(description.description))
        end
      end
      column link_to "Image File" do |about|
        # image_tag(post.image_file_name)
        image_tag(about.image.url, height:'100')
      end
      column :created_at
      column :updated_at
      actions
    end
end
