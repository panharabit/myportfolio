ActiveAdmin.register Contact do
menu parent: "Manage Content"


permit_params :saysomething,:location,:website,:phone,:email,:description

show do |t|
  attributes_table do
    row :saysomething
    row :location
    row :website
    row :phone
    row :email
    row :description do |d|
      d.description
    end
  end
end




  form :html=> {:enctype=>"multipart/form-data"} do |f|
  f.inputs do
    f.input :saysomething
    f.input :location
    f.input :website
    f.input :email
    f.input :description, :as => :ckeditor
    actions
  end

end

index do
  selectable_column
  column link_to "ID" do |contact|
    link_to contact.id,admin_contact_path(contact)
  end
  column :saysomething
  column :location
  column :website
  column :email
  column :description, :sortable => :description do |description|
    div do
      truncate(strip_tags(description.description))
    end
  end
  column :created_at
  column :updated_at
  actions
end

end
