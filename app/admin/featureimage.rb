ActiveAdmin.register Featureimage do

  permit_params :image,:blog_id
    show do |t|
      attributes_table do
        row :image do
          featureimage.image? ? image_tag(featureimage.image.url, height: '100') : content_tag(:span, "No photo yet")
        end
      end
    end


    form :html=> {:enctype=>"multipart/form-data"} do |f|
    f.inputs do
      f.collection_select :blog_id,Blog.all,:id,:title
      f.file_field :image, hint: f.featureimage.image? ? image_tag(featureimage.image.url, height:'100') : content_tag(:span,"Upload JPG/PNG/GIF image")
      actions
    end

  end

  index do
    selectable_column
    column link_to "ID" do |image|
      link_to image.id,admin_featureimage_path(image)
    end
    column link_to "Image File" do |image|
      # image_tag(post.image_file_name)
      image_tag(image.image.url, height:'100')
    end
    column :created_at
    column :updated_at
    actions
  end

end
