ActiveAdmin.register Skill do
menu parent: "Manage Content"
permit_params :title,:poem,:description

show do |t|
  attributes_table do
    row :title
    row :poem
    row :description do |d|
      d.description
    end

  end
end




  form :html=> {:enctype=>"multipart/form-data"} do |f|
  f.inputs do
    f.input :title
    f.input :poem
    f.input :description, :as => :ckeditor
    actions
  end

end

index do
  selectable_column
  column link_to "ID" do |skill|
    link_to skill.id,admin_skill_path(skill)
  end
  column :title
  column :poem
  column :description, :sortable => :description do |description|
    div do
      truncate(strip_tags(description.description))
    end
  end
  column :created_at
  column :updated_at
  actions
end
end
