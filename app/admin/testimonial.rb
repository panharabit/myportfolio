ActiveAdmin.register Testimonial do
menu parent: "Manage Content"
permit_params :name,:image,:description,:position

show do |t|
  attributes_table do
    row :name
    row :postion
    row :description
    row :image do
      testimonial.image? ? image_tag(testimonial.image.url, height: '100') : content_tag(:span, "No photo yet")
    end
  end
end




  form :html=> {:enctype=>"multipart/form-data"} do |f|
  f.inputs do
    f.input :name
    f.input :postion
    f.input :description
    f.file_field :image, hint: f.testimonial.image? ? image_tag(testimonial.image.url, height:'100') : content_tag(:span,"Upload JPG/PNG/GIF image")
    actions
  end

end

index do
  selectable_column
  column link_to "ID" do |test|
    link_to test.id,admin_edu_path(test)
  end
  column :name
  column :postion
  column :description
  column link_to "Image File" do |test|
    # image_tag(post.image_file_name)
    image_tag(test.image.url, height:'100')
  end
  column :created_at
  column :updated_at
  actions
end
end
